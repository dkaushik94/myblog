from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from MainPage.views import *
from blog.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myblog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
 
    url(r'^adminiamdebojit/', include(admin.site.urls)),
    url(r'^main/$',main),
    url(r'^allmyblogs/$',blogpage),
    url(r'^(?P<blog_id>\d+)/$',blogspecific),
    url(r'^createmyblog/$',create_new_blog),
    url(r'^my_portfolio/$',my_portfolio),
    url(r'^edit/(?P<blog_id>\d+)/$',edit_my_blog),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

