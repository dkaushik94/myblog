from django.shortcuts import render

# Create your views here.

def main(request):
	return render(request,'MainPage/index.html')

def my_portfolio(request):
	return render(request,'MainPage/my_portfolio.html')
