from django.db import models
from django.utils.encoding import smart_text
from taggit.managers import TaggableManager
import time
# Create your models here.


#Class for Blogging.
class blog(models.Model):
	blog_title=models.CharField('What do you want the title of the blog to be?',max_length=100,blank=False)
	blog_content=models.TextField('Start writing your content',max_length=100000000,blank=False)
	date=models.DateField(auto_now_add=True)
	time=models.TimeField(auto_now=True)
	blog_image=models.ImageField(upload_to= 'blog_img/', blank=True)
	relevent_fields = TaggableManager('This blog is related to?')
	def __str__(self):
		return smart_text(self.blog_title)

#Class for contacting me.
class contactform(models.Model):
	name = models.CharField("Name of the Person",max_length=100,null=False,blank=False)
	email=models.EmailField(max_length=254,blank=False)
	message=models.CharField(max_length=10000,blank=False)
	host_country=models.CharField(max_length=100,blank=False)
	timestamp=models.DateTimeField(auto_now=True)
	def __str__(self):
		return smart_text(self.email)

#Class for comments.
# class comments(models.Model):
# 	relevent_blog=models.ForeignKey(blog)
# 	name = models.CharField("Your Name:",max_length=100,blank=False)
# 	timestamp=models.DateTimeField(auto_now=True)
# 	message=models.CharField(label="Write your comment:",max_length=100000,blank=False)
# 	def __str__(self):
# 		return smart_text(self.name)
