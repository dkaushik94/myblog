from django.shortcuts import render
from blog.models import *
from django.http import HttpResponse,HttpResponseRedirect
from .forms import newblog,contactf
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import json
# Create your views here.

def blogpage(request):
	blog_object=blog.objects.all()	
	if(request.method == 'POST'):
		form=contactf(request.POST)
		if(form.is_valid()):
			contactobject=form.save(commit=False)
			print(contactobject.name+" "+contactobject.email+" "+contactobject.message)
			contactobject.save()
		else:
			#return(form.errors)
			#if error is not None:
			#	return form.errors
			#raise HTTPResponse404()
            		return HttpResponse(json.dumps(form.errors),content_type='application/json')
	else:	
		form=contactf()
	return render(request,'Blog/blogpage.html',{'blog':blog_object,'request':request,'form':form})
	

def blogspecific(request,blog_id):
	bloged=blog.objects.get(id=blog_id)
	return render(request,'Blog/blogspecific.html',{'blog':bloged,'j':0 ,'request':request})


def create_new_blog(request):
	form=newblog(request.POST or None)
	if (request.method == 'POST'):
		form=newblog(request.POST , request.FILES or None)
		if (form.is_valid()):
			blogger=blog.objects.create()
			blogger.blog_title=form.cleaned_data.get('Blog_title')
			blogger.blog_content=form.cleaned_data.get('Write_your_content')
			blogger.blog_image=request.FILES['Related_blog_image']
			tags=form.cleaned_data['new_relevent_fields']		
			blogger.save()
			for tag in tags:
				blogger.relevent_fields.add(tag)
			return HttpResponseRedirect('/allmyblogs/')
	return render(request,'Blog/new_blog.html',{'form':form})

def edit_my_blog(request,blog_id):
	bloged=blog.objects.get(id=blog_id)
	form=newblog(initial={'Blog_title' : bloged.blog_title, 'Write_your_content' : bloged.blog_content, 'Related_blog_image' : bloged.blog_image,})	
	if (request.method == 'POST'):
		form=newblog(request.POST , request.FILES or None)
		if (form.is_valid()):
			blogger=bloged
			blogger.blog_title=form.cleaned_data.get('Blog_title')
			blogger.blog_content=form.cleaned_data.get('Write_your_content')
			blogger.blog_image=request.FILES['Related_blog_image']
			tags=form.cleaned_data['new_relevent_fields']		
			blogger.save()
			for tag in tags:
				blogger.relevent_fields.add(tag)
			return HttpResponseRedirect('/allmyblogs/')
	return render(request,'Blog/edit_this_blog.html',{'form':form})


