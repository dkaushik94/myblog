from django import forms
from django.forms import ModelForm
from taggit.forms import *
from django.forms.extras.widgets import SelectDateWidget
from blog.models import contactform

#from blog.models import blog
class newblog(forms.Form):
	Blog_title=forms.CharField()
	Write_your_content=forms.CharField(widget=forms.Textarea(attrs={'id':'textarea1', 'class':'materialize-textarea validate'}))
	Related_blog_image=forms.ImageField(widget=forms.ClearableFileInput())	
	new_relevent_fields = TagField()

class contactf(forms.ModelForm):
	class Meta:
		model = contactform
		fields = '__all__'
		widgets = {
			'name': forms.TextInput(attrs={'type':'text','class':'validate','id':'first_name','for':'first_name'}),
			'email': forms.EmailInput(attrs={'class':'validate'}),
			'message':forms.Textarea(attrs={'id':'textarea1', 'class':'materialize-textarea validate'})
		}

		labels = {
			'name':'First Name'
		}
		
# class commentform(forms.ModelForm):
# 	class meta:
# 		model = comments
# 		exclude = ['relevent_blog','timestamp']
