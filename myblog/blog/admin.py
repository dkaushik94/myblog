from django.contrib import admin
from blog.models import *
# Register your models here.

class blogAdmin(admin.ModelAdmin):
	class Meta:
		model = blog

admin.site.register(blog,blogAdmin)
admin.site.register(contactform)
