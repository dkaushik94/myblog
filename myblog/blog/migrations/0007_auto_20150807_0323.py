# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20150807_0309'),
    ]

    operations = [
        migrations.RenameField(
            model_name='blog',
            old_name='date_and_time',
            new_name='date',
        ),
    ]
