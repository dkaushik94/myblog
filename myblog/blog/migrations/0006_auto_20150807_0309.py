# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150806_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='date_and_time',
            field=models.DateField(auto_now_add=True),
        ),
    ]
