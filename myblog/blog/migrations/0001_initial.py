# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='blog',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('blog_title', models.CharField(max_length=100, verbose_name='What do you want the title of the blog to be?')),
                ('blog_content', models.TextField(max_length=100000000, verbose_name='Start writing your content')),
                ('date_and_time', models.DateTimeField(auto_now=True)),
                ('blog_image', models.ImageField(upload_to='')),
            ],
        ),
    ]
