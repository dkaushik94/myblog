# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0012_auto_20150906_1640'),
    ]

    operations = [
        migrations.CreateModel(
            name='comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(verbose_name='Your Name:', max_length=100)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('message', models.CharField(max_length=100000)),
            ],
        ),
        migrations.AddField(
            model_name='contactform',
            name='country',
            field=models.CharField(default=datetime.datetime(2015, 9, 18, 9, 19, 57, 839383, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contactform',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 18, 9, 20, 14, 728675, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
