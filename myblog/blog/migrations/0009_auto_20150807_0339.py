# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_blog_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='time',
            field=models.TimeField(auto_now=True),
        ),
    ]
