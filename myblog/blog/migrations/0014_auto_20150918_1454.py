# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0013_auto_20150918_1450'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contactform',
            old_name='country',
            new_name='host_country',
        ),
    ]
