# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20150807_0323'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='time',
            field=models.TimeField(default='03:35:37'),
        ),
    ]
